import React, {useState, useEffect} from "react"
import {BOARD} from "./constants"
import {Board} from "./components/Board"
import {Form} from "./components/Form"
import {Rover} from "./components/Cell"
import {handleMove, isValidCommand, generateObstacles, showFeedback} from "./utils"
import "./App.css"

BOARD.obstacles = generateObstacles();

export default function App() {
	const [position, setPosition] = useState(BOARD.initialPosition)
	const [direction, setDirection] = useState(BOARD.initialDirection)
	const [command, setCommand] = useState("")
	const [submitCount, setSubmitCount] = useState(0)

	const onCommandChange = (event) => setCommand(event.target.value.toUpperCase())
	const onDirectionChange = (event) => setDirection(event.target.value)
	const onFormSubmit = (event) => {
		event.preventDefault()
		if (isValidCommand(command)) {
			setSubmitCount((submitCount) => submitCount + 1)
			return
		}
		showFeedback(":( Invalid command: please use FLR keys")
	}

	useEffect(() => {
		if (!submitCount) return
		const {newPosition, feedback} = handleMove(position, direction, command)
		setPosition(newPosition)
		showFeedback(feedback)
	}, [submitCount]) // eslint-disable-line react-hooks/exhaustive-deps

	return (
		<div className="App">
			<h1>Mars Rover Mission</h1>

			<Form
				onSubmit={onFormSubmit}
				direction={direction}
				onDirectionChange={onDirectionChange}
				command={command}
				onCommandChange={onCommandChange}
			/>

			<div className="board">
				<Board/>
				<Rover position={position}/>
			</div>
		</div>
	)
}