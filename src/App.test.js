import {render, screen, fireEvent} from "@testing-library/react"
import App from "./App"

const inputPlaceholder = /Forward Left Right/
const selectInitialValue = /⇧ North/

test("renders command form", () => {
	render(<App/>)
	const select = screen.getByDisplayValue(selectInitialValue)
	const input = screen.getByPlaceholderText(inputPlaceholder)
	expect(select).toBeInTheDocument()
	expect(input).toBeInTheDocument()
})

test("renders board table", () => {
	render(<App/>)
	const board = screen.getByRole("table")
	expect(board).toBeInTheDocument()
})

test("can write on command input & transforms to uppercase", () => {
	render(<App/>)
	const lowercaseKey = "f"
	const uppercaseKey = "F"
	const input = screen.getByPlaceholderText(inputPlaceholder)

	fireEvent.change(input, {"target": {"value": lowercaseKey}})
	expect(input).toHaveValue(uppercaseKey)

	fireEvent.change(input, {"target": {"value": uppercaseKey}})
	expect(input).toHaveValue(uppercaseKey)
})

test("can change direction select", () => {
	render(<App/>)
	const value = "W"
	const select = screen.getByDisplayValue(selectInitialValue)
	fireEvent.change(select, {"target": {value}})
	expect(select).toHaveValue(value)
})
