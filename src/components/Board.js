import React from "react"
import {BOARD} from "../constants"
import {Cell} from "./Cell"

export const Board = React.memo(() => {
	let cells = []

	for (let row = (BOARD.size[1] - 1); row >= 0; row--) {
		for (let col = 0; col < BOARD.size[0]; col++) {
			cells.push(
				<Cell
					key={`${col}-${row}`}
					cell={[col, row]}
				/>,
			)
		}
	}

	return (
		<div className="cells" style={{"--cols": BOARD.size[0]}}>
			{cells}
		</div>
	)
})