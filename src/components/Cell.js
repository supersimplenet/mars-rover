import {hasObstacle} from "../utils"

export const Cell = ({cell}) => {
	if (hasObstacle(cell)) return <ObstacleCell/>

	return <EmptyCell/>
}

export const Rover = ({position}) => {
	return (
		<div className="rover" style={{"--left": position[0], "--bottom": position[1]}}>
			M
		</div>
	)
}

const ObstacleCell = () => <div className="cell">X</div>
const EmptyCell = () => <div className="cell">&nbsp;</div>