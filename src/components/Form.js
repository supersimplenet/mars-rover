export const Form = ({onSubmit, direction, onDirectionChange, command, onCommandChange}) => {
	return (
		<form onSubmit={onSubmit}>
			<select value={direction} onChange={onDirectionChange}>
				<option value="N">⇧ North</option>
				<option value="S">⇩ South</option>
				<option value="E">⇨ East</option>
				<option value="W">⇦ West</option>
			</select>

			<input
				type="text"
				value={command}
				onChange={onCommandChange}
				placeholder="Forward Left Right"
			/>

			<button type="submit">GO</button>
		</form>
	)
}