export const BOARD = {
	initialPosition: [0, 0],
	initialDirection: "N",
	size: [200, 200],
	obstaclesPercentage: 20,
}