import {BOARD} from "../constants"

export const hasObstacle = (cell, obstacles = BOARD.obstacles) => {
	return obstacles.some(obstacle => obstacle[0] === cell[0] && obstacle[1] === cell[1])
}

export const handleMove = (position, direction, command) => {
	let newPosition = [...position]

	for (let i = 0; i < command.length; i++) {
		let oldPosition = newPosition
		newPosition = getNewPosition(oldPosition, direction, command[i])

		if (isOffBoard(newPosition)) {
			return {
				newPosition: oldPosition,
				feedback: `:( Out of planet boundaries!\nPartially command executed: ${command.substr(0, i)}`,
			}
		}

		if (hasObstacle(newPosition)) {
			return {
				newPosition: oldPosition,
				feedback: `:( Obstacle found!\nPartially command executed: ${command.substr(0, i)}`,
			}
		}
	}

	return {
		newPosition,
		feedback: ":) Command executed!",
	}
}

export const getNewPosition = (position, direction, step) => {
	let [x, y] = position

	const mapping = {
		"N": {
			"F": [x, y + 1], "L": [x - 1, y], "R": [x + 1, y],
		},
		"S": {
			"F": [x, y - 1], "L": [x + 1, y], "R": [x - 1, y],
		},
		"E": {
			"F": [x + 1, y], "L": [x, y + 1], "R": [x, y - 1],
		},
		"W": {
			"F": [x - 1, y], "L": [x, y - 1], "R": [x, y + 1],
		},
	}

	return mapping?.[direction]?.[step] || position
}

export const isValidCommand = (command) => {
	if (!command || typeof command !== "string") return false
	const match = command.match(new RegExp(/[FLR]/g))
	return match?.length === command.length
}

export const showFeedback = (message) => {
	if (message) alert(message)
}

export const isOffBoard = (position, boardSize = BOARD.size) => {
	return (
		position[0] < 0 ||
		position[0] >= boardSize[0] ||
		position[1] < 0 ||
		position[1] >= boardSize[1]
	)
}

export const generateObstacles = (
	boardSize = BOARD.size,
	reservedCells = [BOARD.initialPosition],
	obstaclesPercentage = BOARD.obstaclesPercentage,
) => {
	const availableCellsCount = (boardSize[0] * boardSize[1]) - reservedCells.length
	const obstacleCellsMax = Math.floor(availableCellsCount * obstaclesPercentage / 100)
	const obstacles = []

	while (obstacles.length < obstacleCellsMax) {
		const x = getRandomNumberBetween(0, boardSize[0])
		const y = getRandomNumberBetween(0, boardSize[1])

		const isDuplicated = obstacles.some(cell => cell[0] === x && cell[1] === y)
		const isReserved = reservedCells.some(cell => cell[0] === x && cell[1] === y)

		if (!isDuplicated && !isReserved) {
			obstacles.push([x, y])
		}
	}

	return obstacles
}

const getRandomNumberBetween = (min, max) => {
	return Math.floor(Math.random() * (max - min) + min)
}