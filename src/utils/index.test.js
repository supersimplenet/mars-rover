import {isValidCommand, isOffBoard, hasObstacle, getNewPosition, generateObstacles} from "./index"

test("isValidCommand returns true only on uppercase FLR", () => {
	expect(isValidCommand()).toBe(false)
	expect(isValidCommand("a")).toBe(false)
	expect(isValidCommand("flr")).toBe(false)
	expect(isValidCommand("Fa")).toBe(false)
	expect(isValidCommand("FLR")).toBe(true)
})

test("isOffBoard returns true only on off board positions", () => {
	expect(isOffBoard([-1, 0])).toBe(true)
	expect(isOffBoard([-1, -1])).toBe(true)
	expect(isOffBoard([4, 4], [5, 5])).toBe(false)
	expect(isOffBoard([5, 5], [5, 5])).toBe(true)
	expect(isOffBoard([0, 0])).toBe(false)
})

test("hasObstacle return true only when there is a match", () => {
	const obstacles = [[1, 1], [2, 3]]
	expect(hasObstacle([], [])).toBe(false)
	expect(hasObstacle([-1, -1], obstacles)).toBe(false)
	expect(hasObstacle([0, 0], obstacles)).toBe(false)
	expect(hasObstacle([1, 1], obstacles)).toBe(true)
	expect(hasObstacle([2, 3], obstacles)).toBe(true)
})

test("getNewPosition to return expected value on all NSEW directions", () => {
	const x = 1
	const y = 2
	expect(getNewPosition([x, y], null, "F")).toEqual([x, y])
	expect(getNewPosition([x, y], null, "R")).toEqual([x, y])
	expect(getNewPosition([x, y], null, "L")).toEqual([x, y])
	expect(getNewPosition([x, y], null, null)).toEqual([x, y])
	expect(getNewPosition([x, y], null, null)).toEqual([x, y])
	expect(getNewPosition([x, y], null, null)).toEqual([x, y])
	expect(getNewPosition([x, y], "N", "F")).toEqual([x, y + 1])
	expect(getNewPosition([x, y], "N", "R")).toEqual([x + 1, y])
	expect(getNewPosition([x, y], "N", "L")).toEqual([x - 1, y])
	expect(getNewPosition([x, y], "S", "F")).toEqual([x, y - 1])
	expect(getNewPosition([x, y], "S", "R")).toEqual([x - 1, y])
	expect(getNewPosition([x, y], "S", "L")).toEqual([x + 1, y])
	expect(getNewPosition([x, y], "E", "F")).toEqual([x + 1, y])
	expect(getNewPosition([x, y], "E", "R")).toEqual([x, y - 1])
	expect(getNewPosition([x, y], "E", "L")).toEqual([x, y + 1])
	expect(getNewPosition([x, y], "W", "F")).toEqual([x - 1, y])
	expect(getNewPosition([x, y], "W", "R")).toEqual([x, y + 1])
	expect(getNewPosition([x, y], "W", "L")).toEqual([x, y - 1])
})

test("generates right amount of obstacles", () => {
	expect(generateObstacles([]).length).toBe(0)
	expect(generateObstacles([1, 1]).length).toBe(0)
	expect(generateObstacles([2, 5]).length).toBe(1)
	expect(generateObstacles([10, 10]).length).toBe(19)
})